﻿//using PizzaV2.ViewModels;
using System;
using MonkeyCache.LiteDB;
using TimeTracker.Config;
using TimeTracker.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {

            InitializeComponent();
            Routing.RegisterRoute(nameof(MainMenuPage), typeof(MainMenuPage));
            Routing.RegisterRoute("../AppShell", typeof(AppShell));

        }

        private async void Button_OnClicked_Login(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.login.Text) || 
                string.IsNullOrWhiteSpace(this.login.Text)
                || string.IsNullOrEmpty(this.mdp.Text) || 
                string.IsNullOrWhiteSpace(this.mdp.Text) )
            {
                await DisplayAlert("Alert", "Login ou Mot de passe incorrecte", "OK");
            }
            else
            {
                
                Barrel.ApplicationId = Constant.KEY_LOGIN;
                Login login = new Login(this.login.Text,mdp.Text);


                GetLoginData loginData = await App.TimerManager.GetTokenLogin(login);

                if (loginData != null)
                {
                    if (Barrel.Current.Exists(login.login))
                    {
                        


                        App.TimerManager.SaveLoginInProperties(login);
                       // await Navigation.PopAsync();
                        var page1 = new Page1();
                        await Navigation.PushAsync(page1);



                    }
                    else
                    {
                        App.TimerManager.SaveLoginInProperties(login);
                        await App.TimerManager.GetAuthentificationToken(login);
                       // await Navigation.PopAsync();
                                           

                    }




                }
                else
                {
                    await DisplayAlert("Alert", "Login ou Mot de passe incorrecte", "OK");
                }


                
            }

        }

    private void Button_OnClicked_Inscription(object sender, EventArgs e)
    {
    Navigation.PushAsync(new RegistrationPage(false));
    }


}

}