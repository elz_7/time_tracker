﻿using System;
using PizzaIllico.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TimeTracker.Services;

namespace TimeTracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuPage : ContentPage
    {
        private TimeTrackerServiceAdmin timeManager;
        public MainMenuPage()
        {
            InitializeComponent();
            timeManager = new TimeTrackerServiceAdmin (new TimeTrackerService());
            
           

        }
        private void Button_OnClicked_Login(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LoginPage());
        }
        private void Button_OnClicked_Inscription(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegistrationPage(false));
        }

    }
}