﻿using System.Threading.Tasks;
using TimeTracker.Models;
using TimeTracker.Services;
//using PizzaV2.ViewModels;

namespace PizzaIllico.Services
{
    public class TimeTrackerServiceAdmin
    {
        private ITimeTrackerService _service;

        public TimeTrackerServiceAdmin(ITimeTrackerService pizzaService)
        {
            this._service = pizzaService;
        }

        public Task<GetLoginData> GetTokenLogin(Login login)
        {
            return _service.GetTokenLogin(login);
        }

        

        public Task<bool> Insription(UserRegister user)
        {
            return _service.Inscription(user);
        }

        public Task<bool> UpdatePassword(UpdatePassword updatePassword, string token)
        {
            return _service.UpdatePassword(updatePassword, token);
        }


      

       
        public Task<GetLoginData> RefreshToken(string token)
        {
            return _service.RefreshToken(token);
        }

        public Task<string> GetAuthentificationToken(Login login)
        {
            return _service.GetAuthentificationToken(login);
        }


        public Login GetLoginInProperties()
        {

            return _service.GetLoginInProperties();
        }

        public void SaveLoginInProperties(Login login)
        {
            _service.SaveLoginInProperties(login);
        }

     

        public Task<GetUser> GetUser(string token)
        {
            return _service.GetUser(token);
        }

        public Task<bool> UpdateProfil(UserUpdate userUpdate, string token)
        {
            return _service.UpdateProfil(userUpdate, token);


        }
    }
}