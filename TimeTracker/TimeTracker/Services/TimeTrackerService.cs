﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using MonkeyCache.LiteDB;
using Newtonsoft.Json;
using TimeTracker.Config;
using TimeTracker.Models;
using Xamarin.Forms;
using JsonSerializer = System.Text.Json.JsonSerializer;
using TimeTracker.Services;

namespace TimeTracker.Services
{
    public class TimeTrackerService : ITimeTrackerService
    {

        HttpClient client;
        JsonSerializerOptions serializerOptions;

        public TimeTrackerService()
        {
            client = new HttpClient();
            serializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
        }

        

        public async Task<bool> Inscription(UserRegister user)
        {
            Uri uri = new Uri(Constant.URL_USER_INSCRIPTION);

            try
            {

                string json = JsonSerializer.Serialize<UserRegister>(user);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PostAsync(uri, content);

                string contentResponse = await response.Content.ReadAsStringAsync();

    
                GetLoginData code =  JsonConvert.DeserializeObject<GetLoginData>(contentResponse);

                return code.is_success;

            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
       
        }

        public async Task<GetLoginData> GetTokenLogin(Login login)
        {
            Uri uri = new Uri(Constant.URL_LOGIN);
            GetLoginData loginData = null;

            try
            {

                string json = JsonSerializer.Serialize<Login>(login, serializerOptions);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PostAsync(uri, content);

                string contentResponse = await response.Content.ReadAsStringAsync();


                if (response.IsSuccessStatusCode)
                {
               
                    loginData=  JsonConvert.DeserializeObject<GetLoginData>(contentResponse);

             
                    Debug.WriteLine(@"\tTodoItem successfully saved.");
                }
                else return null;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"\tERROR {0}", ex.Message);
                
                Debug.WriteLine("IMPOSSIBLE Connect", ex.Message);
            }

            return loginData;
        }


        public async Task<bool>  UpdatePassword(UpdatePassword updatePassword,string token)
        {
            Uri uri = new Uri(Constant.URL_UPDATE_PASSWORD);

            try
            {
                
                string json = JsonSerializer.Serialize<UpdatePassword>(updatePassword);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                
                HttpResponseMessage response = null;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
               
               var method = new HttpMethod("PATCH");

               var request = new HttpRequestMessage(method, uri) {
                   Content = new StringContent(
                       JsonConvert.SerializeObject(updatePassword),
                       Encoding.UTF8, "application/json")
               };
                response = await client.SendAsync(request);
          
                string contentResponse = await response.Content.ReadAsStringAsync();
                GetLoginData code =  JsonConvert.DeserializeObject<GetLoginData>(contentResponse);

                return code.is_success;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<string> GetAuthentificationToken(Login login)
        {

            try
            {
                var key = login.login;
                //Dev handles checking if cache is expired
                if(!Barrel.Current.IsExpired(key))
                {
                    var   d =Barrel.Current.Get<GetLoginData>(key: key);
                    return d.data.access_token;
                }
                else if (Barrel.Current.Exists(key) && Barrel.Current.IsExpired(key: key))
                {
                    GetLoginData loginDataRes = Barrel.Current.Get<GetLoginData>(key: key);
                    loginDataRes = await RefreshToken(loginDataRes.data.refresh_token);
                    if (loginDataRes != null)
                    {
                        Barrel.Current.Add(key: key, data: loginDataRes, expireIn: TimeSpan.FromSeconds(loginDataRes.data.expires_in));
                        return loginDataRes.data.access_token;
                    }             
                }
                //Saves the cache and pass it a timespan for expiration
         
    
                GetLoginData dataUser=  await GetTokenLogin(login);
           
                Barrel.Current.Add(key: key, data: dataUser, expireIn: TimeSpan.FromSeconds(5));
                return dataUser.data.access_token;
            }
            catch (Exception e)
            {
                return null;
            }  
        }
        public async Task<GetLoginData> RefreshToken(string token)
        {
            RefreshToken refreshToken = new RefreshToken(token);
            Uri uri = new Uri(Constant.URL_REFRESH_TOKEN);
            GetLoginData loginData = null;

            try
            {

                string json = JsonSerializer.Serialize<RefreshToken>(refreshToken);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PostAsync(uri, content);

                string contentResponse = await response.Content.ReadAsStringAsync();


                if (response.IsSuccessStatusCode)
                {
                   
                    loginData=  JsonConvert.DeserializeObject<GetLoginData>(contentResponse);

                
                    Debug.WriteLine(@"\tTodoItem successfully saved.");
                }
                else return null;

            }
            catch (Exception ex)
            {
              
            }

            return loginData;
        }

       public Login GetLoginInProperties()
       {
           string key = Constant.KEY_LOGIN;
            if (Application.Current.Properties.ContainsKey(key))
            {
                string value = Application.Current.Properties[key].ToString();

                Login login = JsonConvert.DeserializeObject<Login>(value);
                return login;
            }

            return null;

        }

       public void SaveLoginInProperties(Login login)
       {
           string key = Constant.KEY_LOGIN;
           string valueJson = JsonConvert.SerializeObject(login);
            
           Application.Current.Properties[key] = valueJson;
           Application.Current.SavePropertiesAsync();
       }
      

       public async Task<GetUser> GetUser(string token)
       {
           Uri uri = new Uri(Constant.URL_GET_USER);
           GetUser loginData = null;

           try
           {
               client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
               
               HttpResponseMessage response = null;
               response = await client.GetAsync(uri);

               string contentResponse = await response.Content.ReadAsStringAsync();


               if (response.IsSuccessStatusCode)
               {
             
                   loginData=  JsonConvert.DeserializeObject<GetUser>(contentResponse);
                   return loginData;

                
               }
               else return null;

           }
           catch (Exception ex)
           {
              
           }

           return loginData;
       }

       public  async Task<bool> UpdateProfil(UserUpdate userUpdate,string token)
       {
           Uri uri = new Uri(Constant.URL_UPDATE_PROFIL);

           try
           {
                
               string json = JsonSerializer.Serialize<UserUpdate>(userUpdate);
               StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                
               HttpResponseMessage response = null;
               client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
               
               var method = new HttpMethod("PATCH");

               var request = new HttpRequestMessage(method, uri) {
                   Content = new StringContent(
                       JsonConvert.SerializeObject(userUpdate),
                       Encoding.UTF8, "application/json")
               };
               response = await client.SendAsync(request);
          
               string contentResponse = await response.Content.ReadAsStringAsync();
               GetUser code =  JsonConvert.DeserializeObject<GetUser>(contentResponse);

               return code.is_success;


           }
           catch (Exception ex)
           {
           

               return false;
           }
       }


    }
    
 
   
    
}

