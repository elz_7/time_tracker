﻿using System.Threading.Tasks;
using TimeTracker.Models;

namespace TimeTracker.Services
{
    public interface ITimeTrackerService
    {
    
        /**
         * Prend en paramètre un utilisation et le sauvegarde dans la base
         * de données
         */
        Task <bool> Inscription(UserRegister user);
        
        /**
         * Cette méthode permets de récuperer le token et le refresh token 
         */
        Task<GetLoginData> GetTokenLogin(Login login);
       
       
        Task <bool> UpdatePassword(UpdatePassword updatePassword,string token);

    
        Task<GetLoginData> RefreshToken(string token);
      /**
       * retourne le token ou un nouveau token si celui ci est expiré
       * le token et sa durée de validité est stoké en local
       */
         Task<string> GetAuthentificationToken(Login login);
        /**
         * retoune le login et le mot de passe de l'utilisateur
         */

         Login GetLoginInProperties();
        /**
         * stocke le login en local
         */
         void SaveLoginInProperties(Login login);
       
          
          Task <bool> UpdateProfil(UserUpdate userUpdate,string token);
        /**
         * retourne le profile d'un utilisateur
         */

          Task<GetUser> GetUser(string token);

    }
}