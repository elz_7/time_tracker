﻿namespace TimeTracker.Models
{
    public class DataToken
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; } 

      
    }
}