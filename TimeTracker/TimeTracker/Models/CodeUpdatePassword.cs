﻿namespace TimeTracker.Models
{
    public class CodeUpdatePassword
    {
        public bool is_success { get; set; }
        public bool error_code { get; set; }
        public bool error_message { get; set; }
    }
}