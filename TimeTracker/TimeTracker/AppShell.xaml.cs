﻿//using PizzaV2.ViewModels;
using TimeTracker.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TimeTracker
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
        }

        private async void GoToLogOut(object sender, EventArgs e)
        {
            Shell.Current.FlyoutIsPresented = false;
            await Navigation.PushAsync(new LoginPage());
        }

       

        private async void GoToEditPassword(object sender, EventArgs e)
        {
            Shell.Current.FlyoutIsPresented = false;
            await Navigation.PushAsync(new EditPasswordPage());
        
        }

        private async void GoToRegistration(object sender, EventArgs e)
        {
            Shell.Current.FlyoutIsPresented = false;
            await Navigation.PushAsync(new RegistrationPage(false));
        }

       

        private async void GoToConnect(object sender, EventArgs e)
        {
            Shell.Current.FlyoutIsPresented = false;
            await Navigation.PushAsync(new LoginPage());
        }

        private  async void GoToEditProfile(object sender, EventArgs e)
        {
            Shell.Current.FlyoutIsPresented = false;
            await Navigation.PushAsync(new RegistrationPage(true));
        }
    }
}
