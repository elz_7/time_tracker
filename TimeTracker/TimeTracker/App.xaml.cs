﻿
using TimeTracker.Views;
using System;
using PizzaIllico.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TimeTracker.Services;

namespace TimeTracker
{
    public partial class App : Application
    {
        public static TimeTrackerServiceAdmin TimerManager { get; private set; }

        public App()
        {
            InitializeComponent();

      
           MainPage = new AppShell();
      
            TimerManager = new TimeTrackerServiceAdmin (new TimeTrackerService());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
